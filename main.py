import sqlite3
import csv
from cmd import Cmd

conn = sqlite3.connect('sqdb')
cursor = conn.cursor()


class Menu(Cmd):
    intro = "Type 'start' to start. \
        \n Press 1 to list everyone \
        \n Press 2 to list by first name \
        \n Press 3 to list by last name \
        \n Press 4 to list by birth date \
        \n Press 5 to list by address \
        \n Press 6 to delete someone"
    
    prompt = "What next?  "
    
    def do_start(self, arg):
        try:
            conn.execute("""CREATE TABLE if not exists peoples (
                firstname text,
                lastname Text,
                address text,
                dateofbirth int,
                unique_id text
                )""")
            
            with open('People.csv')as file:
                contents = csv.reader(file)
                insert_records = "INSERT INTO peoples (firstname, lastname, address, dateofbirth, unique_id) VALUES(?,?,?,?,?)"
                cursor.executemany(insert_records, contents)
                conn.commit()
                print("Start successeful.")

        except Exception as e:
            print("ERROR: "+str(e))
        

    
    def do_1(self,arg):
        try:
            cursor.execute("SELECT * FROM peoples ORDER BY unique_id")
            rows = cursor.fetchall()
            for x in rows:
                print(x)
        
        except Exception as e:
            print("ERROR: "+str(e))


    def do_2(self,arg):
        try:
            cursor.execute("SELECT firstname FROM peoples")
            rows = cursor.fetchall()
            for x in rows:
                print(x)
        
        except Exception as e:
            print("ERROR: "+str(e))

    
    def do_3(self,arg):
        try:
            cursor.execute("SELECT lastname FROM peoples")
            rows = cursor.fetchall()
            for x in rows:
                print(x)
        
        except Exception as e:
            print("ERROR: "+str(e))
            
    
    
    def do_4(self,arg):
        try:
            cursor.execute("SELECT address FROM peoples")
            rows = cursor.fetchall()
            for x in rows:
                print(x)
        
        except Exception as e:
            print("ERROR: "+str(e))
            
    
    def do_5(self,arg):
        try:
            cursor.execute("SELECT dateofbirth FROM peoples")
            rows = cursor.fetchall()
            for x in rows:
                print(x)
        
        except Exception as e:
            print("ERROR: "+str(e))

        
    def do_6(self,arg):
        try:
            cursor.execute("SELECT * FROM peoples")
            delete = int(input("Input Unique ID of person: "))
            rows = cursor.fetchall()
            cursor.execute(f"DELETE FROM peoples WHERE unique_id = {delete}")
            print("Person has been deleted")
        
        except Exception as e:
            print("ERROR: "+str(e))


if __name__ == '__main__':
    Menu().cmdloop()

